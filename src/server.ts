import express from "express";
import { PORT, PostgresPort } from "./constants";
import studentRouter from "./router/studentRouter"
import teacherRouter from "./router/teacherRouter"
import testRouter from "./router/testRouter"
import subjectRouter from "./router/subjectRouter"
const app = express();
app.use(express.json());

app.use('/api/v1/student', studentRouter);
app.use('/api/v1/teacher', teacherRouter);
app.use('/api/v1/test', testRouter);
app.use('/api/v1/subject', subjectRouter);

// TEACHER

const server = app.listen(PORT, () => {
    console.log(`Localhost đã chạy ở port ${PORT}`);
});
