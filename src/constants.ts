import * as dotenv from "dotenv";

dotenv.config();

if (!process.env.PORT) // validate undefined
{
    process.exit(1);
}

export const PORT: number = parseInt(process.env.PORT);

// if (process.env.PostgresPort == undefined) postgresPort = "5432"
// else postgresPort = process.env.PostgresPort
// process.env.PostgresPort ?? "5432"
// process.env.PostgresPort == undefined ? "5432" : process.env.PostgresPort

export const PostgresPort: number = parseInt(process.env.PostgresPort ?? "5432");
export const PostgresHost: string = process.env.PostgresHost ?? "";
export const PostgresUser: string = process.env.PostgresUser ?? "";
export const PostgresPass: string = process.env.PostgresPass ?? "";
export const PostgresSchema: string = process.env.PostgresSchema ?? "";
