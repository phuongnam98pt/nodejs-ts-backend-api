import { PostgresPort, PostgresHost, PostgresUser, PostgresPass, PostgresSchema } from "../constants";
import Client from 'pg';

const Pool = Client.Pool;
export const pool = new Pool({
    user: PostgresUser,
    host: PostgresHost,
    database: PostgresSchema,
    password: PostgresPass,
    port: PostgresPort,
})