import express, { Request, Response } from "express";
import { pool } from './baseController';
import { validate } from "../validation";

export const getTeacher = (request: Request, response: Response) => {
    try {
        pool.query(`SELECT * FROM teacher LEFT JOIN subject
        ON teacher.subjectid = subject.id; `, (error, results) => {
            if (error) {
                return response.json(
                    {
                        status: false,
                        message: "something went wrong"
                    }
                )
            }
            return response.json(
                {
                    status: true,
                    data: results.rows,
                    message: ""
                }
            )
        })
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message
        return response.json(
            {
                status: false,
                message: message
            }
        )
    }

}

export const getTeacherWithTest = async (request: Request, response: Response) => {
    try {

        let requestData = request.body;
        var valid = validate(requestData, {
            testId: { type: "string" },
        });

        if (valid.status == false) {
            return response.json({
                status: false,
                message: valid.errors[0],
            });
        }
        requestData = valid.data;
        var queryParam = []
        const testId = requestData.testId ? requestData.testId : null;
        var query = `SELECT teacher.*, "test".id as "testID", "test"."testName" as "testName" FROM teacher left join "test" on teacher.id = "test"."teacherid" 
        where "test"."id" `;
        testId == null ? `is null` : `= $1`
        if (testId == null) {
            query += `is null`
        } else {
            query += `= $1`;
            queryParam.push(testId)
        }
        const createDate: Date = new Date();

        var results = await pool.query(query,
            queryParam);
        return response.json(
            {
                status: true,
                data: results.rows,
                message: ""
            }
        )

    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message
        return response.json(
            {
                status: false,
                message: message
            }
        )
    }

}

export const createTeacher = async (request: Request, response: Response) => {
    try {
        let requestData = request.body;

        var valid = validate(requestData, {
            name: { type: "string", required: true },
            code: { type: "string", required: true },
            birthday: { type: "string", required: true },
            gender: { type: "string", required: true },
            address: { type: "string", required: true },
            phone: { type: "string", required: true },
            email: { type: "string", required: true },
            subjectid: { type: "number", required: true },

        });

        if (valid.status == false) {
            return response.json({
                status: false,
                message: valid.errors[0],
            });
        }
        requestData = valid.data;

        const name = requestData.name ? requestData.name : "";
        const code = requestData.code ? requestData.code : "";
        const birthday = requestData.birthday ? requestData.birthday : "";
        const gender = requestData.gender ? requestData.gender : "";
        const address = requestData.address ? requestData.address : "";
        const phone = requestData.phone ? requestData.phone : "";
        const email = requestData.email ? requestData.email : "";
        const subjectid = requestData.subjectid ? requestData.subjectid : "";

        const createDate: Date = new Date();

        // check subjectId exist 
        var resultsSubject = await pool.query(`SELECT id FROM subject`);
        var subjectIds = resultsSubject.rows;
        var checkSubjectIdExit = subjectIds.some(item => item.id === subjectid)
        if (!checkSubjectIdExit) {
            return response.json(
                {
                    status: false,
                    message: "subject does not exist"
                }
            )
        }
        // step 1
        var results = await pool.query(`INSERT INTO Teacher 
        ("teacherName", code, birthday, "createdDate", gender, address, phone, email, subjectid) VALUES 
        ($1, $2, $3, $4, $5, $6, $7, $8, $9) `,
            [name, code, birthday, createDate, gender, address, phone, email, subjectid]);

        // step 2
        return response.json(
            {
                status: true,
                message: ""
            }
        )
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message

        return response.json(
            {
                status: false,
                message: message
            }
        )
    }

}
export const updateTeacher = async (request: Request, response: Response) => {
    try {
        let requestData = request.body;

        var valid = validate(requestData, {
            id: { type: "string", required: true },
            name: { type: "string", required: true },
            code: { type: "string", required: true },
            birthday: { type: "string", required: true },
            gender: { type: "string", required: true },
            address: { type: "string", required: true },
            phone: { type: "string", required: true },
            email: { type: "string", required: true },
            subjectid: { type: "number", required: true },

        });

        if (valid.status == false) {
            return response.json({
                status: false,
                message: valid.errors[0],
            });
        }
        requestData = valid.data;

        const id = requestData.id ? requestData.id : "";
        const name = requestData.name ? requestData.name : "";
        const code = requestData.code ? requestData.code : "";
        const birthday = requestData.birthday ? requestData.birthday : "";
        const gender = requestData.gender ? requestData.gender : "";
        const address = requestData.address ? requestData.address : "";
        const phone = requestData.phone ? requestData.phone : "";
        const email = requestData.email ? requestData.email : "";
        const subjectid = requestData.subjectid ? requestData.subjectid : "";

        const createDate: number = new Date().getTime();

        var results = await pool.query('UPDATE Teacher SET "teacherName" = $1, code = $2, birthday = $3, gender = $4, address = $5, phone = $6, email = $7, subjectid = $8 WHERE id = $9',
            [name, code, birthday, gender, address, phone, email, subjectid, id]
        )
        if (results.rows.length == 0) {
            return response.json(
                {
                    status: false,
                    message: "no record found"
                }
            )
        }
        return response.json(
            {
                status: true,
                message: ""
            }
        )
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message

        return response.json(
            {
                status: false,
                message: message
            }
        )
    }

}
export const deleteTeacher = async (request: Request, response: Response) => {
    try {
        let requestData = request.body;

        var valid = validate(requestData, {
            id: { type: "string", required: true },
        });

        if (valid.status == false) {
            return response.json({
                status: false,
                message: valid.errors[0],
            });
        }
        requestData = valid.data;
        const id = requestData.id ? requestData.id : "";
        // check teacherId exist 
        var resultsTeacher = await pool.query(`SELECT id FROM teacher`);
        var teacherIds = resultsTeacher.rows;
        var checkTeacherExit = teacherIds.some(item => item.id === id)
        if (!checkTeacherExit) {
            return response.json(
                {
                    status: false,
                    message: "Teacher does not exist"
                }
            )
        }
        var results = await pool.query('DELETE FROM Teacher WHERE id = $1', [id], (error, results) => {
            if (error) {
                return response.json(
                    {
                        status: false,
                        message: "something went wrong"
                    }
                )
            }
            return response.json(
                {
                    status: true,
                    message: ""
                }
            )
        })
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message

        return response.json(
            {
                status: false,
                message: message
            }
        )
    }

}