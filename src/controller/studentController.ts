import express, { Request, Response } from "express";
import { pool } from './baseController';
import { validate } from "../validation";

export const getStudent = (request: Request, response: Response) => {
    try {
        pool.query('SELECT * FROM student', (error, results) => {
            if (error) {
                return response.json(
                    {
                        status: false,
                        message: "something went wrong"
                    }
                )
            }
            return response.json(
                {
                    status: true,
                    data: results.rows,
                    message: ""
                }
            )
        })
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message
        return response.json(
            {
                status: false,
                message: message
            }
        )
    }

}
export const getStudentWithTest = async (request: Request, response: Response) => {
    try {

        let requestData = request.body;
        var valid = validate(requestData, {
            testId: { type: "string", required: true },
        });

        if (valid.status == false) {
            return response.json({
                status: false,
                message: valid.errors[0],
            });
        }
        requestData = valid.data;

        const testId = requestData.testId ? requestData.testId : "";

        const createDate: Date = new Date();

        var results = await pool.query(`SELECT * FROM student inner join "studentTest" on student.id = "studentTest".studentid 
        inner join "test" on "studentTest".testid = "test".id
        where testid = $1`,
            [testId]);
        return response.json(
            {
                status: true,
                data: results.rows,
                message: ""
            }
        )

    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message
        return response.json(
            {
                status: false,
                message: message
            }
        )
    }

}
export const createStudent = async (request: Request, response: Response) => {
    try {
        let requestData = request.body;
        var valid = validate(requestData, {
            name: { type: "string", required: true },
            code: { type: "string", required: true },
            birthday: { type: "string", required: true },
            gender: { type: "string", required: true },
            address: { type: "string", required: true },
            phone: { type: "string", required: true },
            email: { type: "string", required: true },
        });

        if (valid.status == false) {
            return response.json({
                status: false,
                message: valid.errors[0],
            });
        }
        requestData = valid.data;

        const name = requestData.name ? requestData.name : "";
        const code = requestData.code ? requestData.code : "";
        const birthday = requestData.birthday ? requestData.birthday : "";
        const gender = requestData.gender ? requestData.gender : "";
        const address = requestData.address ? requestData.address : "";
        const phone = requestData.phone ? requestData.phone : "";
        const email = requestData.email ? requestData.email : "";

        const createDate: Date = new Date();
        // step 0: validate

        // step 1
        var results = await pool.query(`INSERT INTO student 
        ("name", code, birthday, "createdDate", gender, address, phone, email) VALUES 
        ($1, $2, $3,  $4, $5, $6, $7, $8) `,
            [name, code, birthday, createDate, gender, address, phone, email]);

        // step 2
        return response.json(
            {
                status: true,
                message: ""
            }
        )
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message
        return response.json(
            {
                status: false,
                message: message
            }
        )
    }

}
export const updateStudent = async (request: Request, response: Response) => {
    try {
        let requestData = request.body;

        var valid = validate(requestData, {
            id: { type: "string", required: true },
            name: { type: "string", required: true },
            code: { type: "string", required: true },
            birthday: { type: "string", required: true },
            gender: { type: "string", required: true },
            address: { type: "string", required: true },
            phone: { type: "string", required: true },
            email: { type: "string", required: true },
        });

        if (valid.status == false) {
            return response.json({
                status: false,

                message: valid.errors[0],
            });
        }
        requestData = valid.data;

        const id = requestData.id ? requestData.id : "";
        const name = requestData.name ? requestData.name : "";
        const code = requestData.code ? requestData.code : "";
        const birthday = requestData.birthday ? requestData.birthday : "";
        const gender = requestData.gender ? requestData.gender : "";
        const address = requestData.address ? requestData.address : "";
        const phone = requestData.phone ? requestData.phone : "";
        const email = requestData.email ? requestData.email : "";
        const createDate: number = new Date().getTime();

        var results = await pool.query(`
        UPDATE public.student
        SET "name" = $1, code = $2, birthday = $3, gender = $4, address = $5, phone = $6, email = $7 
        WHERE id = $8`,
            [name, code, birthday, gender, address, phone, email, id])
        return response.json(
            {
                status: true,
                message: ""
            }
        )
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message

        return response.json(
            {
                status: false,
                message: message
            }
        )
    }

}
export const deleteStudent = async (request: Request, response: Response) => {
    try {
        let requestData = request.body;

        var valid = validate(requestData, {
            id: { type: "string", required: true },
        });

        if (valid.status == false) {
            return response.json({
                status: false,
                message: valid.errors[0],
            });
        }
        requestData = valid.data;
        const id = requestData.id ? requestData.id : "";

        var results = await pool.query('DELETE FROM student WHERE id = $1', [id], (error, results) => {
            if (error) {
                return response.json(
                    {
                        status: false,
                        message: "something went wrong"
                    }
                )
            }
            return response.json(
                {
                    status: true,
                    message: ""
                }
            )
        })
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message

        return response.json(
            {
                status: false,
                message: message
            }
        )
    }

}