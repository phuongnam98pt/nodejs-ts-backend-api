import express, { Request, Response } from "express";
import { pool } from './baseController';
import { validate } from "../validation";

export const getSubject = (request: Request, response: Response) => {
    try {
        pool.query('SELECT * FROM Subject', (error, results) => {
            if (error) {
                return response.json(
                    {
                        status: false,
                        message: "something went wrong"
                    }
                )
            }
            return response.json(
                {
                    status: true,
                    data: results.rows,
                    message: ""
                }
            )
        })
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message
        return response.json(
            {
                status: false,
                message: message
            }
        )
    }

}
export const createSubject = async (request: Request, response: Response) => {
    try {
        let requestData = request.body;
        var valid = validate(requestData, {
            name: { type: "string", required: true }
        });

        if (valid.status == false) {
            return response.json({
                status: false,
                message: valid.errors[0],
            });
        }
        requestData = valid.data;

        const name = requestData.name ? requestData.name : "";

        const createDate: Date = new Date();
        // step 0: validate

        // step 1
        var results = await pool.query(`INSERT INTO Subject 
        ( "subjectName") VALUES 
        ($1) `,
            [name]);

        // step 2
        return response.json(
            {
                status: true,
                message: ""
            }
        )
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message
        return response.json(
            {
                status: false,
                message: message
            }
        )
    }

}
export const updateSubject = async (request: Request, response: Response) => {
    try {
        let requestData = request.body;
        var valid = validate(requestData, {
            id: { type: "string", required: true },
            name: { type: "string", required: true }

        });

        if (valid.status == false) {
            return response.json({
                status: false,
                message: valid.errors[0],
            });
        }
        requestData = valid.data;

        const id = requestData.id ? requestData.id : "";
        const name = requestData.name ? requestData.name : "";

        const createDate: Date = new Date();

        var results = await pool.query('UPDATE Subject SET "subjectName" = $1 WHERE id = $4',
            [name, id], (error, results) => {
                if (error) {
                    return response.json(
                        {
                            status: false,
                            message: "something went wrong"
                        }
                    )
                }
                return response.json(
                    {
                        status: true,
                        message: ""
                    }
                )
            })
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message

        return response.json(
            {
                status: false,
                message: message
            }
        )
    }

}
export const deleteSubject = async (request: Request, response: Response) => {
    try {
        let requestData = request.body;

        var valid = validate(requestData, {
            id: { type: "string", required: true },
        });

        if (valid.status == false) {
            return response.json({
                status: false,
                message: valid.errors[0],
            });
        }
        requestData = valid.data;
        const id = requestData.id ? requestData.id : "";

        var results = await pool.query('DELETE FROM Subject WHERE id = $1', [id], (error, results) => {
            if (error) {
                return response.json(
                    {
                        status: false,
                        message: "something went wrong"
                    }
                )
            }
            return response.json(
                {
                    status: true,
                    message: ""
                }
            )
        })
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message

        return response.json(
            {
                status: false,
                message: message
            }
        )
    }

}