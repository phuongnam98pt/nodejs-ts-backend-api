import express, { Request, Response } from "express";
import { pool } from './baseController';
import { validate } from "../validation";

export const getTest = (request: Request, response: Response) => {
    try {
        pool.query('SELECT * FROM Test', (error, results) => {
            if (error) {
                return response.json(
                    {
                        status: false,
                        message: "something went wrong"
                    }
                )
            }
            return response.json(
                {
                    status: true,
                    data: results.rows,
                    message: ""
                }
            )
        })
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message
        return response.json(
            {
                status: false,
                message: message
            }
        )
    }

}
export const createTest = async (request: Request, response: Response) => {
    try {
        let requestData = request.body;
        var valid = validate(requestData, {
            name: { type: "string", required: true },
            teacherId: { type: "number", required: true },
        });

        if (valid.status == false) {
            return response.json({
                status: false,
                message: valid.errors[0],
            });
        }
        requestData = valid.data;

        const name = requestData.name ? requestData.name : "";
        const teacherId = requestData.teacherId ? requestData.teacherId : "";

        const createDate: Date = new Date();

        // check teacherId exist 
        var resultsTeacher = await pool.query(`SELECT id FROM teacher`);
        var teacherIds = resultsTeacher.rows;
        var checkTeacherExit = teacherIds.some(item => item.id === teacherId)
        if (!checkTeacherExit) {
            return response.json(
                {
                    status: false,
                    message: "Teacher does not exist"
                }
            )
        }

        // step 1
        var results = await pool.query(`INSERT INTO Test 
        (teacherid, "testName") VALUES 
        ($1, $2) `,
            [teacherId, name]);

        // step 2
        return response.json(
            {
                status: true,
                message: ""
            }
        )
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message
        return response.json(
            {
                status: false,
                message: message
            }
        )
    }

}
export const updateTest = async (request: Request, response: Response) => {
    try {
        let requestData = request.body;
        var valid = validate(requestData, {
            id: { type: "string", required: true },
            name: { type: "string", required: true },
            teacherId: { type: "number", required: true },

        });

        if (valid.status == false) {
            return response.json({
                status: false,
                message: valid.errors[0],
            });
        }
        requestData = valid.data;

        const id = requestData.id ? requestData.id : "";
        const name = requestData.name ? requestData.name : "";
        const teacherId = requestData.teacherId ? requestData.teacherId : "";

        const createDate: Date = new Date();

        var results = await pool.query('UPDATE Test SET "testName" = $1, teacherId = $2 WHERE id = $3',
            [name, teacherId, id], (error, results) => {
                if (error) {
                    return response.json(
                        {
                            status: false,
                            message: "something went wrong"
                        }
                    )
                }
                return response.json(
                    {
                        status: true,
                        message: ""
                    }
                )
            })
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message

        return response.json(
            {
                status: false,
                message: message
            }
        )
    }

}
export const deleteTest = async (request: Request, response: Response) => {
    try {
        let requestData = request.body;

        var valid = validate(requestData, {
            id: { type: "string", required: true },
        });

        if (valid.status == false) {
            return response.json({
                status: false,
                message: valid.errors[0],
            });
        }
        requestData = valid.data;
        const id = requestData.id ? requestData.id : "";

        var results = await pool.query('DELETE FROM Test WHERE id = $1', [id], (error, results) => {
            if (error) {
                return response.json(
                    {
                        status: false,
                        message: "something went wrong"
                    }
                )
            }
            return response.json(
                {
                    status: true,
                    message: ""
                }
            )
        })
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message

        return response.json(
            {
                status: false,
                message: message
            }
        )
    }

}