import express from "express";
import { createSubject, deleteSubject, getSubject, updateSubject } from "../controller/subjectController";

const router = express.Router();

router.post('/get', getSubject);
router.post('/create', createSubject);
router.post('/update', updateSubject);
router.post('/delete', deleteSubject);

export default router;


