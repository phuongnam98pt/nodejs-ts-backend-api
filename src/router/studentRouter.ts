import express from "express";
import { createStudent, deleteStudent, getStudent, getStudentWithTest, updateStudent } from "../controller/studentController";

const router = express.Router();

router.post('/get', getStudent);
router.post('/getStudentWithTest', getStudentWithTest);
router.post('/create', createStudent);
router.post('/update', updateStudent);
router.post('/delete', deleteStudent);

export default router;


