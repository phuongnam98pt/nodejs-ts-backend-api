import express from "express";
import { createTeacher, deleteTeacher, getTeacher, getTeacherWithTest, updateTeacher } from "../controller/teacherController";

const router = express.Router();

router.post('/get', getTeacher);
router.post('/getTeacherWithTest', getTeacherWithTest);
router.post('/create', createTeacher);
router.post('/update', updateTeacher);
router.post('/delete', deleteTeacher);

export default router;


