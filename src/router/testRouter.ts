import express from "express";
import { createTest, deleteTest, getTest, updateTest } from "../controller/testController";

const router = express.Router();

router.post('/get', getTest);
router.post('/create', createTest);
router.post('/update', updateTest);
router.post('/delete', deleteTest);

export default router;


